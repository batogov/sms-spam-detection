from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import cross_val_score
from sklearn.cross_validation import ShuffleSplit

from sklearn.feature_extraction.text import TfidfVectorizer

from sklearn.linear_model import SGDClassifier
from sklearn.naive_bayes import MultinomialNB

from nltk.stem.porter import PorterStemmer

import pandas as pd
import re


def text_preparation(text):
    # текст в lowercase + удаляем из него всё, кроме букв и цифр
    text = re.sub('[^\w\s]', '', text.lower())
    # заменяем все числа на "1"
    text = re.sub('\d+', '1', text)
    # удаляем множественные пробелы
    text = re.sub(' +', ' ', text)

    stemmer = PorterStemmer()

    prepared_text = []
    for word in text.split():
        prepared_text.append(stemmer.stem(word))

    return ' '.join(prepared_text)


# работа с данными - считывание и преобразование
data = pd.read_csv('data.txt', sep='\t', names=('label', 'text'), header=None)
data['text'] = data['text'].apply(text_preparation)


y = data['label']

cv = ShuffleSplit(len(y), n_iter=5, test_size=0.2, random_state=0)

linear_parameters = {'loss': ('log', 'hinge'),
                         'penalty': ['none', 'l1', 'l2', 'elasticnet'],
                         'alpha': [0.001, 0.0001, 0.00001, 0.000001]
}

ngrams = [(1, 1), (1, 2), (1, 3), (1, 4)]

for ngram in ngrams:
    tfidf_vectorizer = TfidfVectorizer(analyzer = "word", ngram_range=ngram)
    X = tfidf_vectorizer.fit_transform(data['text'])

    linear_clf = SGDClassifier()
    gs_linear_clf = GridSearchCV(linear_clf, linear_parameters, cv=cv, n_jobs=-1)
    gs_linear_clf.fit(X, y)

    nb_clf = MultinomialNB()
    nb_scores = cross_val_score(nb_clf, X, y, cv=cv, n_jobs=-1)

    print('N-gram: ({}, {})'.format(ngram[0], ngram[1]))
    print('Linear best parameters:', gs_linear_clf.best_params_)
    print('Linear best score:', gs_linear_clf.best_score_)
    print('NB mean score:', nb_scores.mean())
